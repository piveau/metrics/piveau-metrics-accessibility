package io.piveau.metrics.accessibility

import io.vertx.core.http.HttpClientResponse
import io.vertx.core.http.HttpMethod
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.client.HttpResponse

class Status private constructor(
    val method: HttpMethod? = null,
    val response: Boolean = false,
    val code: Int? = null,
    val message: String? = null,
    val reason: String? = null,
) {

    fun asJsonObject(url: String): JsonObject {
        val obj = JsonObject()
            .put("url", url)
            .put("response", response)
        method?.let { obj.put("method", method.name()) }
        code?.let { obj.put("code", code) }
        message?.let { obj.put("message", message) }
        reason?.let { obj.put("reason", reason) }
        return obj
    }

    companion object {
        fun create(response: HttpResponse<Void>): Status = Status(
            method = HttpMethod.HEAD,
            response = true,
            code = response.statusCode(),
            message = response.statusMessage(),
        )

        fun create(response: HttpClientResponse): Status = Status(
            method = HttpMethod.GET,
            response = true,
            code = response.statusCode(),
            message = response.statusMessage()
        )

        fun create(cause: Throwable, method: HttpMethod? = null): Status = Status(
            method = method,
            response = false,
            reason = cause.message
        )

    }
}