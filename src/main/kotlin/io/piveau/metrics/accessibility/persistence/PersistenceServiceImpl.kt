package io.piveau.metrics.accessibility.persistence

import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import io.vertx.ext.mongo.CountOptions
import io.vertx.ext.mongo.FindOptions
import io.vertx.ext.mongo.MongoClient
import io.vertx.ext.mongo.UpdateOptions
import org.slf4j.LoggerFactory
import java.time.Instant

/**
 * Service implementation to manage persistence of objects in a mongo DB
 */
class PersistenceServiceImpl(private val mongoClient: MongoClient, persistenceConfig: JsonObject = JsonObject()) :
    PersistenceService {

    private val log = LoggerFactory.getLogger(javaClass)

    private val collection = persistenceConfig.getString("collection", "datasets")
    private val maxActiveObjects = persistenceConfig.getLong("maxActiveObjects", 10)

    private val sortingOldest = JsonObject().put("timestamp", 1)

    /**
     * Store a new object
     *
     * The object is stored with a timestamp and the status 'pending'
     */
    override fun storeObject(obj: JsonObject): Future<Void> {
        val query = JsonObject().put("_id", obj.remove("_id").toString())
        obj.put("timestamp", JsonObject().put("\$date", Instant.now()))
        obj.put("status", "pending")
        val update = JsonObject().put("\$set", obj)
        return mongoClient.updateCollectionWithOptions(collection, query, update, UpdateOptions(true))
            .onSuccess { log.debug("Object stored with id ${query.getString("_id")}") }
            .mapEmpty()
    }

    /**
     * Remove object
     *
     * Remove object with given id
     */
    override fun removeObject(objId: String): Future<Void> =
        mongoClient.removeDocument(collection, JsonObject().put("_id", objId)).mapEmpty()

    /**
     * Get next objects to process
     *
     * Returns next n oldest and not active objects, with n as difference between already active objects and maxActiveObjects.
     * At the same time the state of the returned objects are changed to 'active'.
     */
    override fun nextObjects(): Future<List<JsonObject>> =
        mongoClient.countWithOptions(collection, activeQuery, CountOptions())
            .compose { activeObjects ->
                val slots = maxActiveObjects - activeObjects
                val nextActive = mutableListOf<Future<JsonObject>>()
                repeat(slots.toInt()) {
                    nextActive.add(
                        mongoClient.findOneAndUpdateWithOptions(
                            collection,
                            notActiveQuery,
                            activeUpdate,
                            FindOptions().setSort(sortingOldest),
                            UpdateOptions()
                        )
                    )
                }
                Future.join(nextActive).transform { _ ->
                    Future.succeededFuture(nextActive.filter { it.succeeded() && it.result() != null }.map { it.result() })
                }
            }

    companion object {
        internal val activeQuery = JsonObject().put("status", JsonObject().put("\$eq", "active"))
        private val notActiveQuery = JsonObject().put("status", JsonObject().put("\$eq", "pending"))
        private val activeUpdate = JsonObject().put("\$set", JsonObject().put("status", "active"))
        internal val pendingUpdate = JsonObject().put("\$set", JsonObject().put("status", "pending"))
    }

}