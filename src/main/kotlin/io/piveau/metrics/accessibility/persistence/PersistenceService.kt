package io.piveau.metrics.accessibility.persistence

import io.vertx.codegen.annotations.GenIgnore
import io.vertx.codegen.annotations.ProxyGen
import io.vertx.core.Future
import io.vertx.core.Vertx
import io.vertx.core.eventbus.DeliveryOptions
import io.vertx.core.json.JsonObject
import io.vertx.ext.mongo.MongoClient

/**
 * Service to manage persistence of objects
 *
 * It provides methods to store, remove, resetting all objects to pending state and getting next available objects.
 * The service ensures that only a specific amount of objects is in active state
 */
@ProxyGen
interface PersistenceService {

    /**
     * Store a new object
     *
     * The object is stored with a timestamp and the status 'pending'
     */
    fun storeObject(obj: JsonObject): Future<Void>

    /**
     * Remove object
     *
     * Remove object with given id
     */
    fun removeObject(objId: String): Future<Void>

    /**
     * Get next objects to process
     *
     * Returns next n oldest and not active objects, with n as difference between already active objects and maxActiveObjects.
     * At the same time the state of the returned objects are changed to 'active'.
     */
    fun nextObjects(): Future<List<JsonObject>>

    @GenIgnore
    companion object {
        const val ADDRESS = "io.piveau.metrics.accessibility.persistence.service"

        fun create(mongoClient: MongoClient, persistenceConfig: JsonObject): Future<PersistenceService> = Future.future { promise ->
            promise.complete(PersistenceServiceImpl(mongoClient, persistenceConfig))
        }

        fun createProxy(vertx: Vertx) = PersistenceServiceVertxEBProxy(vertx, ADDRESS, DeliveryOptions())
    }

}