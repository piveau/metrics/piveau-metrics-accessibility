package io.piveau.metrics.accessibility.dispatcher

import io.piveau.metrics.accessibility.access.AccessService
import io.piveau.metrics.accessibility.datasetDistributions
import io.vertx.core.CompositeFuture
import io.vertx.core.Future
import io.vertx.core.json.JsonObject
import org.apache.jena.rdf.model.Model
import org.apache.jena.rdf.model.ModelFactory
import org.slf4j.LoggerFactory

sealed class Dispatcher(internal val obj: JsonObject, private val accessService: AccessService) {
    internal val log = LoggerFactory.getLogger(javaClass)

    fun dispatch(): CompositeFuture {
        val model = extractModel()

        val futures = model.datasetDistributions()
            .flatMap { it.accessURLs + it.downloadURLs }
            .map { accessService.queueUrl(it) }

        return Future.join(futures).onComplete {
            // Separate succeeded from failed
            val (succeeded, failed) = futures.partition { it.succeeded() }

            processSucceeded(succeeded)
            processFailed(failed)

            complete()
        }
    }

    abstract fun extractModel(): Model

    open fun processSucceeded(succeededFutures: List<Future<JsonObject>>) { /* Do nothing here */ }

    open fun processFailed(failedFutures: List<Future<JsonObject>>) { /* Do nothing here */ }

    abstract fun complete()
}

class NopeDispatcher(obj: JsonObject, accessService: AccessService) : Dispatcher(obj, accessService) {
    // Simply return an empty model
    override fun extractModel(): Model = ModelFactory.createDefaultModel()

    override fun complete() { /* Do nothing here */ }
}