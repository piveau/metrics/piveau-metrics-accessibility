package io.piveau.metrics.accessibility

import io.piveau.metrics.accessibility.access.ExpiringListener
import io.vertx.core.Vertx
import io.vertx.junit5.Timeout
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import kotlinx.coroutines.runBlocking
import org.ehcache.config.builders.CacheEventListenerConfigurationBuilder
import org.ehcache.config.builders.ExpiryPolicyBuilder
import org.ehcache.config.builders.ResourcePoolsBuilder
import org.ehcache.config.builders.UserManagedCacheBuilder
import org.ehcache.config.units.EntryUnit
import org.ehcache.event.EventType
import org.ehcache.impl.events.CacheEventAdapter
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class CachingTest {

    class EListener: CacheEventAdapter<String, VertxTestContext>() {
        val log = LoggerFactory.getLogger(javaClass)

        override fun onExpiry(key: String?, expiredValue: VertxTestContext?) {
            log.info("Cache entry expired: {} - {}", key, expiredValue)
            expiredValue?.completeNow()
        }

        override fun onEviction(key: String?, evictedValue: VertxTestContext?) {
            log.info("Cache entry evicted: {} - {}", key, evictedValue)
            evictedValue?.completeNow()
        }

        override fun onCreation(key: String?, createdValue: VertxTestContext?) {
            log.info("Cache entry created: {} - {}", key, createdValue)
        }

    }

    @Test
    @Timeout(value = 3, timeUnit = TimeUnit.MINUTES)
    fun `Testing cache event listener`(vertx: Vertx, testContext: VertxTestContext) {
        val userManagedCache =
            UserManagedCacheBuilder.newUserManagedCacheBuilder(String::class.java, VertxTestContext::class.java)
                .withResourcePools(ResourcePoolsBuilder.newResourcePoolsBuilder().heap(500, EntryUnit.ENTRIES))
                .withEventExecutors(Executors.newSingleThreadExecutor(), Executors.newFixedThreadPool(5))
                .withEventListeners(
                    CacheEventListenerConfigurationBuilder
                        .newEventListenerConfiguration(EListener(), EventType.EXPIRED, EventType.EVICTED)
                        .asynchronous()
                        .unordered()
                )
                .withExpiry(
                    ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofMinutes(1)))
                .build(true);

        userManagedCache.put("Test", testContext)

        vertx.setPeriodic(5000) { _ ->
            val count = userManagedCache.count()
            println("Test contained in cache: $count")
        }
    }

}