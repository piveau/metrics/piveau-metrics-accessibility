package io.piveau.metrics.accessibility.persistence

import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith

@DisplayName("Testing persistence service")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class PersistenceServiceTest {

    private lateinit var persistenceService: PersistenceService

    @BeforeAll
    fun setUp(vertx: Vertx, testContext: VertxTestContext) {
        vertx.deployVerticle(PersistenceVerticle::class.java, DeploymentOptions())
            .onSuccess { persistenceService = PersistenceService.createProxy(vertx) }
            .onComplete(testContext.succeedingThenComplete())
    }

    @Test
    fun `Test store object`(vertx: Vertx, testContext: VertxTestContext) {
        val obj = JsonObject()
            .put("_id", "anyId")
            .put("first", "another value")
            .put("second", 300)
        persistenceService.storeObject(obj)
            .onSuccess { testContext.completeNow() }
            .onFailure(testContext::failNow)
    }

    @Test
    fun `Test next objects`(vertx: Vertx, testContext: VertxTestContext) {
        val obj = JsonObject()
            .put("_id", "anotherId")
            .put("first", "another value")
            .put("second", 400)
        persistenceService.storeObject(obj)
            .compose { persistenceService.nextObjects() }
            .onSuccess { list ->
                list.forEach { println(it.encodePrettily()) }
                testContext.completeNow()
            }
            .onFailure(testContext::failNow)
    }

}