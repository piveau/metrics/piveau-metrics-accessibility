package io.piveau.metrics.accessibility.access

import io.piveau.dcatap.DCATAPUriSchema
import io.piveau.dcatap.TripleStore
import io.piveau.metrics.accessibility.datasetDistributions
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.junit5.Timeout
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import io.vertx.kotlin.core.json.jsonObjectOf
import io.vertx.kotlin.coroutines.await
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.transform
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith
import java.util.concurrent.TimeUnit
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

@DisplayName("Testing download service")
@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class AccessServiceTest {

    private lateinit var accessService: AccessService

    @BeforeAll
    fun setUp(vertx: Vertx, testContext: VertxTestContext) {
        vertx.deployVerticle(AccessVerticle::class.java, DeploymentOptions())
            .onSuccess {
                accessService = AccessService.createProxy(vertx)
            }
            .onComplete(testContext.succeedingThenComplete())
    }

    @Test
    @OptIn(ExperimentalTime::class)
    @Timeout(value = 5, timeUnit = TimeUnit.MINUTES)
    fun `Test access to urls`(vertx: Vertx, testContext: VertxTestContext) = runBlocking {
        val catalogueId = "data-norge-no"

        DCATAPUriSchema.config = jsonObjectOf(
            "baseUri" to "http://data.europa.eu/88u/",
            "datasetContext" to "dataset/",
            "distributionContext" to "distribution/",
            "recordContext" to "record/",
            "catalogueContext" to "catalogue/",
            "metricsContext" to "metrics/",
            "historyMetricsContext" to "history_metrics/"
        )

        val tripleStore = TripleStore(vertx, jsonObjectOf("address" to "https://data.europa.eu"))

        val datasets = tripleStore.catalogueManager.allDatasets(catalogueId).await()
        println("Found ${datasets.size} datasets in $catalogueId")

        val list = mutableListOf<JsonObject>()

        measureTime {
            datasets
                .take(100)
                .asFlow()
                .map { tripleStore.datasetManager.getGraph(it.graphNameRef).await() }
                .transform { model ->
                    model.datasetDistributions().forEach { (accessURLs, downloadURLs, _) ->
                        downloadURLs.forEach {
                            emit(it)
                        }
                        accessURLs.forEach {
                            emit(it)
                        }
                    }
                }
                .collect { url ->
                    try {
                        val status = accessService.queueUrl(url).await()
                        list.add(status)
                    } catch (e: Exception) {
                        println(e.message)
                    }
                }
        }.let { duration ->
            println(duration)
        }

        val groups = list.groupBy { it.getInteger("code", -1) }
        groups.forEach { (code, responses) ->
            println("$code: ${responses.size}")
        }
        groups[400]?.let { noResponse ->
            println("Bad requests:")
            noResponse.forEach { println(it.encodePrettily()) }
        }
        groups[-1]?.let { noResponse ->
            println("No response reasons:")
            noResponse.forEach { println(it.encodePrettily()) }
        }
        testContext.completeNow()
    }

}