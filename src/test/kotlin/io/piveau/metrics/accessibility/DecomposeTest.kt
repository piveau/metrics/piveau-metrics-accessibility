package io.piveau.metrics.accessibility

import io.vertx.core.Vertx
import io.vertx.ext.web.client.WebClient
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import org.junit.jupiter.api.Disabled
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.jupiter.api.extension.ExtendWith

@ExtendWith(VertxExtension::class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
class DecomposeTest {

    @Test
    fun `Test decompose extension function`(vertx: Vertx, testContext: VertxTestContext) {
        val (host, port, path, queryParams, ssl) = "https://hotell.difi.no/download/bufdir/rapporteringsenheter?download&second=value".decomposeRequest()

        val request = WebClient.create(vertx).head(port, host, path).ssl(ssl)
        queryParams.forEach { (key, value) -> request.queryParams().set(key, value) }
        println(request.queryParams())
        testContext.completeNow()
    }

}